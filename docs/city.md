# City

- [Settlements – d20PFSRD](https://www.d20pfsrd.com/gamemastering/other-rules/kingdom-building/settlements/)

### [How to randomly generate a village or town for old school D&D game? - Role-playing Games Stack Exchange](https://rpg.stackexchange.com/questions/1568/how-to-randomly-generate-a-village-or-town-for-old-school-dd-game)

[Medieval Demographics](http://www.batintheattic.com/downloads/Medieval%20Demographics%20Made%20Easy.pdf) (PDF File) is one of my goto documents on generating material.

Medieval Demographics discusses two areas of interest. One how to calculate the population of a realm. Two how to calculate the number of shops in a settlement.

Both area use straight forward math to calculate the figures. Both area are based on the author's research using medieval sources and medieval data.

There are several sites that [do the calculations for you such this one on Donjon](https://donjon.bin.sh/fantasy/demographics/)

The principle for villages is straight forward. Figure out how many people it would take to support a viable business then divide that factor into the populate. The whole number is the number of establishments that exist. The remainder is the percentage chance that one additional establishment of that type exists.

S John Ross has been criticized for using unrepresentative data that was found in Life in a Medieval City which is based on a tax census called the [Paris Tax Roll](http://heraldry.sca.org/names/parisbynames.html).

I created an alternative take more suited for Fantasy setting [at this link](http://www.batintheattic.com/downloads/Fantasy%20Demographics%20Version%201.pdf). It is meant to work with [this price list](http://www.batintheattic.com/downloads/MajesticWilderlandsPrice%20List.pdf). [This is a spreadsheet](http://www.batintheattic.com/downloads/Tax%20roll%20Paris%201292%20Rev%202.xls) showing I used the paris tax roll to derive my take.

- [A question of size. How big is reasonable? (Towns, cities, etc) : dndnext](https://www.reddit.com/r/dndnext/comments/5qvvhq/a_question_of_size_how_big_is_reasonable_towns/)
-
