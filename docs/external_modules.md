# External Modules

- ChanceJS
  - Random generator helper for JavaScript
  - http://chancejs.com/
  - https://www.npmjs.com/package/chance
  - https://github.com/chancejs/chancejs
- FakerJS
  - Generate massive amounts of fake data in Node.js and the browser
  - https://www.npmjs.com/package/Faker
  - https://github.com/marak/Faker.js/
- Moniker
  - Random name generator for Node.js
  - [GitHub - weaver/moniker: Random name generator for Node.js](https://github.com/weaver/moniker)
- Sentencer
  - madlibs-style sentence templating in Javascript
  - [GitHub - kylestetz/Sentencer: madlibs-style sentence templating in Javascript](https://github.com/kylestetz/Sentencer)
- seedrandom.js
  - seeded random number generator for Javascript
  - [GitHub - davidbau/seedrandom: seeded random number generator for Javascript](https://github.com/davidbau/seedrandom)
- random
  - The most RANDOM module on npm!
  - [GitHub - transitive-bullshit/random](https://github.com/transitive-bullshit/random)

