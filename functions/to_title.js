export default function to_title(title) {
  title = title.replace(/_/,' ');
  //title = title.replace(/\./,', ');
  title = title.split(' ').map( title_part => {
    if ( ! ['of', 'the'].includes( title_part ) ) {
      title_part = title_part.charAt(0).toUpperCase() + title_part.slice(1);
    }
    return title_part;
  }).join(' ');
  return title;
}
