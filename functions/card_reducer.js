import to_title from './to_title';
import { update as update_character } from '../mk_card/mk_character';
import { update as update_filter } from '../mk_card/mk_filter';
import { update as update_group } from '../mk_card/mk_group';
import { update as update_relationship } from '../mk_card/mk_relationship';
import { update as update_sample } from '../mk_card/mk_sample';

const update = {
  'character': update_character,
  'filter': update_filter,
  'group': update_group,
  'relationship': update_relationship,
  'sample': update_sample,
};

export default function(rrpgg, old_card, action) {
  if ( ! old_card ) {
    throw Error('No card provided');
  }
  action = action || {};
  let card = old_card ;// || {};
  card.deck = card.deck || {};
  card.parents = card.parents || {};
  card.parent_ids = Object.keys(card.parents).map(Number);
  card.children = card.children || {};
  card.children_ids = Object.keys(card.children).map(Number);
  card.children_ids_show = card.children_ids_show || [];
  card.connections = card.connections || {};
  card.connection_ids = Object.keys(card.connections).map(Number);
  card.links = card.links || {};
  card.link_ids = Object.keys(card.links).map(Number);
  card.backlinks = card.backlinks || {};
  card.backlink_ids = Object.keys(card.backlinks).map(Number);

  switch (action.type) {
  case 'add_empty_deck': {
    break;
  }
  case 'shuffle_card': {
    let old_card = Object.assign({}, card.children[action.id] );
    let id = old_card.id;
    let type = old_card.type;
    let new_card = rrpgg.card_new(type);
    new_card.id = id;
    card.children[id] = new_card;
    if ( card.deck.type === 'story' ) {
      let cards = rrpgg.story(card.deck);
      card.deck = rrpgg.deck(null,{type:'new_from_cards',cards,deck_type:card.deck.type});
    } else {
      card.deck = rrpgg.deck(card.deck,{type: 'sort'});
    }

    break;
  }
  case 'delete_child': {
    let id = action.id;
    delete card.children[id];
    card.children_ids.splice(card.children_ids.indexOf(id),1);
    break;
  }
  case 'add_parent_only': {
    if ( action.card ) {
      let target_card = action.card;
      card.parents[target_card.id] = target_card;
    }
    break;
  }
  case 'add_child': {
    if ( action.card ) {
      let target_card = action.card;
      card.children[target_card.id] = target_card;
      target_card = rrpgg.card(target_card,{type:'add_parent_only', card:card});
    }
    if ( action.cards ) {
      let cards = action.cards;
      if ( cards.constructor === Object ) {
        Object.keys(cards).forEach( card_id => {
          let target_card = cards[card_id];
          card = rrpgg.card(card,{type:'add_child',card:target_card});
        });
      } else if ( cards.constructor === Array ) {
        cards.forEach( target_card => {
          card = rrpgg.card(card,{type:'add_child',card:target_card});
        });
      }
    }
    break;
  }
  case 'add_child_only': {
    if ( action.card ) {
      let target_card = action.card;
      card.children[target_card.id] = target_card;
    }
    if ( action.cards ) {
      let cards = action.cards;
      if ( cards.constructor === Object ) {
        Object.keys(cards).forEach( card_id => {
          let target_card = cards[card_id];
          card = rrpgg.card(card,{type:'add_child_only',card:target_card});
        });
      } else if ( cards.constructor === Array ) {
        cards.forEach( target_card => {
          card = rrpgg.card(card,{type:'add_child_only',card:target_card});
        });
      }
    }
    break;
  }
  case 'add_link': {
    if ( action.card ) {
      let target_card = action.card;
      card.links[target_card.id] = target_card;
      target_card.backlinks[card.id] = card;
      target_card = rrpgg.card(target_card); // update target_card
    }
    if ( action.cards ) {
      let cards = action.cards;
      if ( cards.constructor === Object ) {
        Object.keys(cards).forEach( card_id => {
          let target_card = cards[card_id];
          card = rrpgg.card(card,{type:'add_link',card:target_card});
        });
      } else if ( cards.constructor === Array ) {
        cards.forEach( target_card => {
          card = rrpgg.card(card,{type:'add_link',card:target_card});
        });
      }
    }
    break;
  }
  case 'add_connection': {
    if ( action.card ) {
      let target_card = action.card;
      card.connections[target_card.id] = target_card;
      target_card.connections[card.id] = card;
      target_card = rrpgg.card(target_card); // update target_card
    }
    if ( action.cards ) {
      let cards = action.cards;
      if ( cards.constructor === Object ) {
        Object.keys(cards).forEach( card_id => {
          let target_card = cards[card_id];
          card = rrpgg.card(card,{type:'add_connection',card:target_card});
        });
      } else if ( cards.constructor === Array ) {
        cards.forEach( target_card => {
          card = rrpgg.card(card,{type:'add_connection',card:target_card});
        });
      }
    }
    break;
  }
  case 'add_connection_oneway': {
    if ( action.card ) {
      let target_card = action.card;
      card.connections[target_card.id] = target_card;
      //target_card.connections[card.id] = card;
      //target_card = rrpgg.card(target_card); // update target_card
    }
    if ( action.cards ) {
      let cards = action.cards;
      if ( cards.constructor === Object ) {
        Object.keys(cards).forEach( card_id => {
          let target_card = cards[card_id];
          card = rrpgg.card(card,{type:'add_connection_oneway',card:target_card});
        });
      } else if ( cards.constructor === Array ) {
        cards.forEach( target_card => {
          card = rrpgg.card(card,{type:'add_connection_oneway',card:target_card});
        });
      }
    }
    break;
  }
  case 'append_description': {
    let new_line = action.line;
    card.description_lines.push( new_line );
    card.description = card.description_lines.join('\n'); // TODO:figure out how to handle this with edited card.description.
    break;
  }
  case 'add_data': {
    let new_data = action.data;
    card.data = Object.assign(card.data,new_data);
    break;
  }
  case 'new_card': {
    let card_type = action.card_type;
    let config = action;
    let new_card = rrpgg.card_new(card_type, config);
    card = rrpgg.card(card,{type:'add_child',card:new_card});
    break;
  }

  /*
  case 'sort': {
    let sort_groups = {
      'filter': 1,
      'group': 2,
    };
    let card_id_groups = [];
    let card_id_no_group = [];
    card.children_ids.sort( (a,b) => b - a );
    //let card_ids_new = [];
    //let card_ids_deck = [];
    card.children_ids.forEach( id => {
      let to_filter_card = card.children[id];
      if ( sort_groups[to_filter_card.type] !== undefined ) {
        let priority = sort_groups[to_filter_card.type];
        card_id_groups[priority] = card_id_groups[priority] || [];
        card_id_groups[priority].push(id);
        card_id_groups.filter( list => list !== undefined );
      } else {
        card_id_no_group.push(id)
      }

      if ( card.children[id].new ) {
        card_ids_new.push(id);
      } else {
        card_ids_deck.push(id);
      }
      //card.children_ids = card_ids_new.concat(card_ids_deck);
    });
    card.children_ids_show = card_id_groups.reduce( (accumulator, current) => current.concat(accumulator) ).concat(card_id_no_group);
    break;
  }
  */
  }

  card.parent_ids = Object.keys(card.parents).map(Number);
  card.children_ids = Object.keys(card.children).map(Number);
  card.connection_ids = Object.keys(card.connections).map(Number);
  card.link_ids = Object.keys(card.links).map(Number);
  card.backlink_ids = Object.keys(card.backlinks).map(Number);

  let sort_groups = {
    'filter': 10,
    'group': 20,
    'building': 40,
    'all': 50,
    'relationship': 60,
  };
  let card_id_groups = {};
  card.children_ids.sort( (a,b) => b - a );
  card.children_ids.forEach( id => {
    let to_filter_card = card.children[id];
    let priority = sort_groups['all'];
    if ( sort_groups[to_filter_card.type] !== undefined ) {
      priority = sort_groups[to_filter_card.type];
    }
    card_id_groups[priority] = card_id_groups[priority] || [];
    card_id_groups[priority].push(id);
  });
  //card_id_groups.filter( list => list !== undefined );
  let card_id_groups_array = Object.keys(card_id_groups).map( card_id => card_id_groups[card_id] );
  card.children_ids_show = card_id_groups_array.reduce( (accumulator, current) => accumulator.concat(current),[] );

  card_id_groups = [];
  card.connection_ids.sort( (a,b) => b - a );
  card.connection_ids.forEach( id => {
    let to_filter_card = card.connections[id];
    let priority = sort_groups['all'];
    if ( sort_groups[to_filter_card.type] !== undefined ) {
      priority = sort_groups[to_filter_card.type];
    }
    card_id_groups[priority] = card_id_groups[priority] || [];
    card_id_groups[priority].push(id);
  });
  //card_id_groups.filter( list => list !== undefined );
  card_id_groups_array = Object.keys(card_id_groups).map( card_id => card_id_groups[card_id] );
  card.connection_ids_show = card_id_groups_array.reduce( (accumulator, current) => accumulator.concat(current),[] );
  //*/

  if ( update[card.type] !== undefined ) {
    card = update[card.type](rrpgg,card);
  }

  return card;
}
