export default function get_unique_from_list(rrpgg, list_name, n) {
  n = n || 3;
  let words = [];
  let s = 0;
  while ( words.length < n && s<30 ) {
    let word = rrpgg.from_list(list_name);
    if ( ! words.includes( word ) ) {
      words.push( word );
    }
    s++;
  }
  return words;
}
