import { ref_list_names } from '../mk_card/mk_reference';

export function update(rrpgg, card) {
  return card;
}

export default function(rrpgg) {
  let library = {
    by_id: {},
    lists: [],
    group_cards: {},
    card: rrpgg.card_new('group.reference',{
      name: 'References',
      icon_type: 'reference',
    }),
  };
  ref_list_names.forEach( list_name => {
    let ref_group = rrpgg.card_new('reference_group',{list_name});
    library.card.add_child(ref_group);
    library.group_cards[list_name] = ref_group;
    library.lists[list_name] = library.lists[list_name] || [];
    ref_group.children_ids.forEach( card_id => {
      let ref_card = ref_group.children[card_id];
      library.lists[list_name].push(ref_card);
      library.by_id[ref_card.id] = ref_card;
    });
  });
  return library;
}
