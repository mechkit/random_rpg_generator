var lists = require('./lists.json');

lists['city_first_parts'] = lists['city_first_parts'].concat( lists['city_parts'] );
lists['city_second_parts'] = lists['city_second_parts'].concat( lists['city_parts'] );

lists['smells'] = lists['smells'].map( smell_string => {
  return smell_string.split('-')[0].trim();
});

['item', 'armor', 'melee', 'ranged'].forEach( list_name => {
  list_name = `loot_${list_name}_specials`;
  lists[list_name+'_descriptions'] = {};
  lists[list_name] = lists[list_name].map( entry => {
    let entry_name = entry.split('|')[0].trim();
    if ( entry.split('|')[1] !== undefined ) {
      let entry_description = entry.split('|')[1].trim();
      if ( entry_description ) {
        lists[list_name+'_descriptions'][entry_name] = entry_description;
      }
    }
    return entry_name;
  });
});

lists['city_industry_names'] = Object.keys(lists['city_industry']);

//Object.keys(lists).forEach( list_name => {
[
  //'bad_odds',
  'class',
  //'d6',
  'decktet',
  //'decktet_suits',
  //'dust',
  //'enneagram',
  //'even_odds',
  'futhorc',
  //'good_odds',
  //'oblique_strategies',
  //'oddities',
  //'personality_drivers',
  //'playing_card_meaning_rank',
  //'playing_card_meaning_suite',
  'playing_cards',
  //'robots',
  //'rogue_items',
  'story',
  'symbols',
  'tarot',
  //'useless',
].forEach( list_name => {
  //console.log(list_name);
  if ( lists[list_name][0].split('|').length > 0 ){
    //console.log(list_name,lists[list_name])
    lists[list_name] = lists[list_name].map( item => item.split('|').map(str=>str.trim()) );
  }
});
// console.log( Object.keys(lists) )

export default lists;

