const vowels = ['a','e','i','o','u'];

function ana(word) {
  var a = 'a';
  if ( vowels.includes(word[0]) ) {
    a = 'an';
  }
  return a;
}

export default function(rrpgg) {
  var description;
  let object;
  let object_type = rrpgg.pick_one(['container','trinket']);
  rrpgg.pick_one([
    ()=>{
      let simple_object = rrpgg.from_list(`quirk_objects_${object_type}_material`);
      let material = rrpgg.from_list('quirk_materials');
      object = `${material} ${simple_object}`;
    },
    ()=>{
      let special_object = rrpgg.from_list(`quirk_objects_${object_type}`);
      object = `${special_object}`;
    },
  ]).call();
  //const adjective = rrpgg.from_list('adjectives_item');
  let a = ana(object);
  a = a[0].toUpperCase() + a.slice(1);

  let does;
  if ( object_type === 'container' ) {
    does = rrpgg.pick_one(['verb','that','contains','alerts']);
  } else if ( object_type === 'trinket' ) {
    does = rrpgg.pick_one(['verb','that','alerts']);
  }
  if ( does === 'verb' ) {
    let verb_obj = rrpgg.from_list('quirk_verbs');
    let verb = verb_obj.word;
    let target_list = verb_obj.targets;
    let target = rrpgg.from_list(`quirk_${target_list}`);
    description = `${a} ${object} that ${verb} ${target}.`;
  } else if ( does === 'contains' ) {
    let contains = rrpgg.from_list('quirk_always_contains');
    description = `${a} ${object} that always contains ${contains}.`;
  } else if ( does === 'alerts' ) {
    let alerts = rrpgg.from_list('quirk_alerts');
    let target = rrpgg.from_list('quirk_alerts_when_near');
    description = `${a} ${object} that ${alerts} when near ${target}.`;
  } else if ( does === 'that' ) {
    let that = rrpgg.from_list('quirk_that');
    description = `${a} ${object} that ${that}.`;
  }



  /*
  var suggestions = write_good(description,{
    adverb:false,
    tooWordy:false,
  });
  if ( suggestions.length ) {
    console.log(suggestions);
  }
  */
  return description;
}
