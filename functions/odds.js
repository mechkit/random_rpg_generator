var odds = function(rng, specs){
  var r = rng();
  var likely;
  var result;
  if( specs !== undefined ){
    if ( specs.tags.contains('good_odds') ) {
      likely = 0.75;
    } else if ( specs.tags.contains('even_odds') ) {
      likely = 0.50;
    } else if ( specs.tags.contains('bad_odds') ) {
      likely = 0.25;
    } else {
      likely = 0.50;
    }
  } else {
    likely = 0.5;
  }

  result = 'no';
  if ( r > likely ) {
    result = 'yes';
  }

  return result;
};

module.exports = odds;
