import to_title from './to_title';

function get_unique_from_string(rrpgg, string, n) {
  string = string.toLowerCase();
  string = string.replace(/[.,+'"()]/g,'');
  let word_list = string.split(' ');
  word_list = word_list.filter( word => ! ['the','and'].includes(word) );
  word_list = word_list.filter( word => ! /.*[0-9].*/.test(word)  );
  n = n || 3;
  let words = [];
  let s = 0;
  while ( words.length < n && s<30 ) {
    let word = rrpgg.pick_one(word_list);
    if ( ! words.includes( word ) ) {
      words.push( word );
    }
    s++;
  }
  return words;
}


export default function(rrpgg,string) {
  let words = get_unique_from_string(rrpgg,string,2);
  return to_title( words.join('') );
}
