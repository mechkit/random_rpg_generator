import lists from './lists.json';

export default function(rrpgg, list_name, config){
  config = config || {};
  if ( config.pick_one ) {
    return rrpgg.pick_one( lists[list_name] );
  } else {
    let list = lists[list_name];
    if ( config.convert_to_array ) {
      //console.log(list_name,list)
      let list_item_names;
      if ( list.constructor === Array ) {
        list_item_names = list;
      }
      if ( list.constructor === Object ) {
        list_item_names = Object.keys(list);
      }
    }
    return list;
  }
}

