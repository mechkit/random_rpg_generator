export default {

  /*
  shuffle_card() {
    let old_card = Object.assign({}, this.children[action.id] );
    let id = old_card.id;
    let type = old_card.type;
    let new_card = rrpgg.card_new(type);
    new_card.id = id;
    this.children[id] = new_card;
    if ( this.deck.type === 'story' ) {
      let cards = rrpgg.story(card.deck);
      this.deck = rrpgg.deck(null,{type:'new_from_cards',cards,deck_type:card.deck.type});
    } else {
      this.deck = rrpgg.deck(card.deck,{type: 'sort'});
    }

  },
  */
  delete_child(id) {
    delete this.children[id];
    this.children_ids.splice(this.children_ids.indexOf(id),1);
  },
  add(sub_card_type, cards, both_ways=true) {
    if ( cards.constructor === Object && cards.id !== undefined ) {
      let target_card = cards;
      this[sub_card_type][target_card.id] = target_card;
      if ( both_ways ) {
        let sub_card_counter_type = { // Find the matching place to add a card on the other card
          'children': 'parents',
          'parents': 'children',
          'links': 'backlinks',
          'connections': 'connections',
        }[sub_card_type];
        target_card.add(sub_card_counter_type, this, false);
        target_card.finish();
      }
    } else if ( cards.constructor === Object ) {
      Object.keys(cards).forEach( card_id => {
        let target_card = cards[card_id];
        this.add(sub_card_type,target_card,both_ways);
      });
    } else if ( cards.constructor === Array) {
      cards.forEach( target_card => {
        this.add(sub_card_type,target_card,both_ways);
      });
    }
    this.finish();
  },
  add_child(cards) {
    this.add('children', cards);
  },
  add_child_only(cards) {
    this.add('children', cards, false);
  },
  add_link(cards) {
    this.add('links', cards);
  },
  add_connection(cards) {
    this.add('connections', cards);
  },
  add_connection_oneway(cards) {
    this.add('connections', cards, false);
  },
  add_parent_only(cards) {
    this.add('parents', cards, false);
  },
  append_description(new_line) {
    this.description_lines.push( new_line );
    this.description = this.description_lines.join('\n'); // TODO:figure out how to handle this with edited this.description.
    this.finish();
  },
  add_data(new_data) {
    this.data = Object.assign(this.data,new_data);
    this.finish();
  },
  /*
  new_card(config) {
    let card_type = action.card_type;
    let config = action;
    let new_card = rrpgg.card_new(card_type, config);
    this.add_child(new_card);
  },

  sort() {
    let sort_groups = {
      'filter': 1,
      'group': 2,
    };
    let card_id_groups = [];
    let card_id_no_group = [];
    this.children_ids.sort( (a,b) => b - a );
    //let card_ids_new = [];
    //let card_ids_deck = [];
    this.children_ids.forEach( id => {
      let to_filter_card = this.children[id];
      if ( sort_groups[to_filter_card.type] !== undefined ) {
        let priority = sort_groups[to_filter_card.type];
        card_id_groups[priority] = card_id_groups[priority] || [];
        card_id_groups[priority].push(id);
        card_id_groups.filter( list => list !== undefined );
      } else {
        card_id_no_group.push(id)
      }

      if ( this.children[id].new ) {
        card_ids_new.push(id);
      } else {
        card_ids_deck.push(id);
      }
      //card.children_ids = card_ids_new.concat(card_ids_deck);
    });
    this.children_ids_show = card_id_groups.reduce( (accumulator, current) => current.concat(accumulator) ).concat(card_id_no_group);
  },
  */


  finish() {
    this.backlink_ids = Object.keys(this.backlinks).map(Number);
    this.children_ids = Object.keys(this.children).map(Number);
    this.connection_ids = Object.keys(this.connections).map(Number);
    this.link_ids = Object.keys(this.links).map(Number);
    this.parent_ids = Object.keys(this.parents).map(Number);

    let sort_groups = {
      'filter': 10,
      'group': 20,
      'building': 40,
      'all': 50,
      'relationship': 60,
    };
    let card_id_groups = {};
    this.children_ids.sort( (a,b) => b - a );
    this.children_ids.forEach( id => {
      let to_filter_card = this.children[id];
      let priority = sort_groups['all'];
      if ( sort_groups[to_filter_card.type] !== undefined ) {
        priority = sort_groups[to_filter_card.type];
      }
      card_id_groups[priority] = card_id_groups[priority] || [];
      card_id_groups[priority].push(id);
    },);
    //card_id_groups.filter( list => list !== undefined );
    let card_id_groups_array = Object.keys(card_id_groups).map( card_id => card_id_groups[card_id] );
    this.children_ids_show = card_id_groups_array.reduce( (accumulator, current) => accumulator.concat(current),[] );

    card_id_groups = [];
    this.connection_ids.sort( (a,b) => b - a );
    this.connection_ids.forEach( id => {
      let to_filter_card = this.connections[id];
      let priority = sort_groups['all'];
      if ( sort_groups[to_filter_card.type] !== undefined ) {
        priority = sort_groups[to_filter_card.type];
      }
      card_id_groups[priority] = card_id_groups[priority] || [];
      card_id_groups[priority].push(id);
    },);
    //card_id_groups.filter( list => list !== undefined );
    card_id_groups_array = Object.keys(card_id_groups).map( card_id => card_id_groups[card_id] );
    this.connection_ids_show = card_id_groups_array.reduce( (accumulator, current) => accumulator.concat(current),[] );

    if ( this.update !== undefined ) this.update();
  }
};
