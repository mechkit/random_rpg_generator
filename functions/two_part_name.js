export default function two_part_name(rrpgg, prefix, gap) {
  gap = gap || '';
  let first = rrpgg.from_list(`${prefix}_first_parts`);
  let second = rrpgg.from_list(`${prefix}_second_parts`);
  while ( first === second ) {
    second = rrpgg.from_list(`${prefix}_second_parts`);
  }
  /*
  if ( first[first.length-1] === second[0] ) {
    first = first.slice(0,-1);
  }
  */
  let name = first + gap + second;
  name = name[0].toUpperCase() + name.slice(1);
  return name;

}
