function merge(sorted_a,sorted_b) {
  let sorted = {};
  let type_names_a = Object.keys(sorted_a);
  let type_names_b = Object.keys(sorted_b);
  let type_names = type_names_a.concat(type_names_b);
  type_names.forEach( type_name => {
    sorted[type_name] = [];
    let types_a = sorted_a[type_name] || [];
    let types_b = sorted_b[type_name] || [];
    sorted[type_name] = types_a.concat(types_b);
  });
  return sorted;
}

export default function sort_cards(card) {
  let sorted = {};
  let type = card.sub_type !== undefined ? card.type + '.' + card.sub_type : card.type;
  sorted[type] = sorted[type] || [];
  sorted[type].push( card );
  if ( card.children_ids.length ) {
    card.children_ids.forEach( id => {
      let child_card = card.children[id];
      let sorted_children = sort_cards(child_card);
      sorted = merge(sorted,sorted_children);
    });
  }
  return sorted;
}
