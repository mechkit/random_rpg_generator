var Chance = require('chance');
var seedrandom = require('seedrandom');
var odds = require('./functions/odds');
var get_list = require('./functions/get_list');
import two_part_name from './functions/two_part_name';
import mk_card from './mk_card';
import lists from './functions/lists';
import mk_ref_cards from './functions/mk_ref_cards';

import card_reducer from './functions/card_reducer';

export default function(seed_string){
  seed_string = seed_string || undefined;
  //var chance = Chance(seed_string)
  var chance = new Chance(seed_string);
  var rng = seedrandom(seed_string);

  var rrpgg = {
    state: {
      last_id: 0,
    },
    _source_lists: lists,
    _chance: chance,
    library: false,
    seed_string: function(fresh_start=false) {
      if ( ! fresh_start ) {
        return this._chance.string({ length: 8, casing: 'upper', alpha: true, numeric: true });
      } else {
        let neo_chance = new Chance();
        return neo_chance.string({ length: 8, casing: 'upper', alpha: true, numeric: true });
      }
    },
    odds: function(specs){
      return odds(rng,specs);
    },
    dice: function(dice_type) {
      if ( ['d4','d6','d8','d10','d12','d20','d30','d100',].includes(dice_type) ) {
        return this._chance[dice_type]();
      }
    },
    pick_one: function(array, shuffle=false){
      if ( array.constructor === Object ) {
        let object = array;
        array = Object.keys(object).map( key => [key, object[key] ] );
      }
      if ( shuffle ) {
        let neo_chance = new Chance();
        return neo_chance.pickone(array);
      } else {
        return this._chance.pickone(array);
      }
    },
    shuffle_list: function(array, shuffle=false){
      if ( array.constructor === Object ) {
        let object = array;
        array = Object.keys(object).map( key => [key, object[key] ] );
      }
      if ( shuffle ) {
        let neo_chance = new Chance();
        return neo_chance.shuffle(array);
      } else {
        return this._chance.shuffle(array);
      }
    },
    age: function(type){
      type = type || 'adult';
      return this._chance.age({ type: type });
    },
    two_part_name: function(prefix, gap){
      return two_part_name(this, prefix, gap);
    },
    list: function(list_name, config){
      return get_list(this,list_name, config);
    },
    from_list: function(list_name){
      return get_list(this, list_name, {pick_one:true});
    },
    is_list: function(list_name){
      return lists[list_name] !== undefined;
    },
    card_new: function(type, config){
      config = config || {};
      if ( type !== undefined ) {
        config.id = config.id || this.state.last_id++;
      }
      let card = mk_card(this, type, config);
      return card;
    },
    xcard: function(old_card, action){
      let new_card = card_reducer(this, old_card, action);
      return new_card;
    },
  };
  rrpgg.library = mk_ref_cards(rrpgg);

  return rrpgg;
}

// module.exports = random_rpg_generator;


