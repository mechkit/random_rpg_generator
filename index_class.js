var Chance = require('chance');
var seedrandom = require('seedrandom');
var odds = require('./functions/odds');
var get_list = require('./functions/get_list');
import two_part_name from './functions/two_part_name';
import mk_card from './mk_card';
import lists from './functions/lists';
import mk_ref_cards from './functions/mk_ref_cards';

import card_reducer from './functions/card_reducer';

export default class Random_rpg_generator {
  constructor(seed_string){
    this.seed_string = seed_string || undefined;
    var chance = Chance(seed_string);
    this._chance = chance;
    this.rng = seedrandom(seed_string);
    this.state = {
      last_id: 0,
    };
    this._source_lists = lists,
    //this.library = false;
    this.library = mk_ref_cards(this);
  }

  seed_string() {
    return this._chance.string({ length: 8, casing: 'upper', alpha: true, numeric: true });
  }
  odds(specs){
    return odds(this.rng,specs);
  }
  dice(dice_type) {
    if ( ['d4','d6','d8','d10','d12','d20','d30','d100',].includes(dice_type) ) {
      return this._chance[dice_type]();
    }
  }
  pick_one(array){
    if ( array.constructor === Object ) {
      let object = array;
      array = Object.keys(object).map( key => [key, object[key] ] );
    }
    return this._chance.pickone(array);
  }
  age(type){
    type = type || 'adult';
    return this._chance.age({ type: type });
  }
  two_part_name(prefix, gap){
    return two_part_name(this, prefix, gap);
  }
  list(list_name, config){
    return get_list(this,list_name, config);
  }
  from_list(list_name){
    return get_list(this, list_name, {pick_one:true});
  }
  is_list(list_name){
    return lists[list_name] !== undefined;
  }
  card_new(type, config){
    config = config || {};
    if ( type !== undefined ) {
      config.id = config.id || this.state.last_id++;
    }
    let card = mk_card(this, type, config);
    return card;
  }
  card(old_card, action){
    let new_card = card_reducer(this, old_card, action);
    return new_card;
  }
}



