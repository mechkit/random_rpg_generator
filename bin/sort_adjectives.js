#!/usr/bin/env node
const fs = require('fs');
const path = require('path');
const yaml = require('js-yaml');
const json2yaml = require('json2yaml');

const input_file_path = './lists_src/adjectives.txt';
const output_yaml_path = './lists/adjectives.yaml';

console.log('---');
console.log('getting files');

let file_string = fs.readFileSync(input_file_path, {encoding: 'utf8'});
var line_array = file_string.match(/[^\r\n]+/g);

const sub_lists = {
  all: [
    'character',
    'place',
    'city',
    'item',
    'melee',
    'ranged',
    'tools',
  ],
  people: [
    'character',
    'population',
  ],
  loot: [
    'item',
    'melee',
    'ranged',
    'tools'
  ],
  defense: [
    'item',
    'armor',
  ],
  weapon: [
    'melee',
    'ranged',
  ],
  loc: [
    'city',
    'place',
  ],
};
const sub_lists_list = Object.keys(sub_lists);

var lists = {};

line_array.forEach( line => {
  let [adjective,list_names_string] = line.split(' ');
  //let list_names = ['adjective'];
  let list_names = [];
  if ( list_names_string && list_names_string.length > 0 ) {
    list_names = list_names_string.split(',');
  }
  for (var i = 0; i < list_names.length; i++) {
    let list_name = list_names[i];
    if ( sub_lists_list.includes(list_name) ) {
      list_names = list_names.concat( sub_lists[list_name] );
    } else {
      lists[list_name] = lists[list_name] || [];
      lists[list_name].push(adjective);
    }
  }
});

console.log('saving json');
let yaml_string = json2yaml.stringify(lists);
console.log('into: '+ output_yaml_path );
fs.writeFileSync(output_yaml_path, yaml_string);


