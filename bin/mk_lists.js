#!/usr/bin/env node
const fs = require('fs');
const path = require('path');
const yaml = require('js-yaml');
const list_path = './lists/';
const build_path = './functions/';

console.log('---');
console.log('getting files');

function load_files(input_path, list_arrays){
  list_arrays = list_arrays || {};
  let files = fs.readdirSync(input_path);
  files.forEach( file_name => {
    let file_name_parts = file_name.split('.');
    let ext = file_name_parts.slice(-1)[0];
    let input_file_path = path.join(input_path, file_name);
    if ( fs.lstatSync(input_file_path).isDirectory() ){
      console.log('opening: '+ input_file_path );
      load_files(input_file_path, list_arrays);
    } else {
      console.log('loading: '+ input_file_path );
      file_name = input_file_path.split(/\/|\./).slice(-2)[0];
      var location = input_file_path.split(/\/|\./).slice(1,-2);
      var list_id;
      if( location.length > 0 ){
        list_id = location.join('/') + '/' + file_name;
      } else {
        list_id = file_name;
      }
      let file_string = fs.readFileSync(input_file_path, {encoding: 'utf8'});
      if ( ext === 'yaml' ) {
        var doc = yaml.load(file_string);
        if ( doc.constructor === Object ) {
          Object.keys(doc).forEach( sub_list_name => {
            let list_name = file_name + '_' + sub_list_name;
            list_arrays[list_name] = doc[sub_list_name];
          });
        } else if ( doc.constructor === Array ) {
          list_arrays[list_id] = doc;
        }
      } else if ( ext === 'txt' ) {
        var file_array = file_string.match(/[^\r\n]+/g);
        list_arrays[list_id] = file_array;
      } else {
        console.log('CAN\'T HANDLE:', input_file_path );
      }
    }
  });
  return list_arrays;
}

let output_json_path = path.join(build_path, 'lists.json');
try {
  fs.unlinkSync(output_json_path);
} catch(e) {
  console.log('no:', output_json_path);
}

let list_arrays = load_files(list_path);

console.log('saving json');
let pages_json = JSON.stringify(list_arrays);
console.log('into: '+ output_json_path );
fs.writeFileSync(output_json_path, pages_json);


