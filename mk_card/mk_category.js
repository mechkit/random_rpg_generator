import sort_cards from '../functions/sort_cards';
import to_title from '../functions/to_title';

function update() {
  this.description_lines = [];
  if ( this.children_ids.length ) {
    this.description_lines.push( '- Contains: ' + this.children_ids.length + ' cards' );
  }
  this.description = this.description_lines.join('\n');
  return this;
}

export default function(rrpgg, card, config){
  if ( card === undefined ) return ['category'];
  let card_to_sample = config.card;
  card.name = 'Contents of ' + card_to_sample.name;
  let cards_by_type = sort_cards(card_to_sample);
  card.data.cards_by_type = cards_by_type;
  let card_types = Object.keys(cards_by_type);
  //console.log(card_types)
  card_types.forEach( card_type => {
    let [type, subtype] = card_type.split('.');
    let name = subtype || type;
    name = to_title(name);
    let type_group_card = rrpgg.card_new('group.',{name:name});
    card.add_child(type_group_card);
    cards_by_type[card_type].forEach( target_card => {
      type_group_card.add_connection_oneway(target_card);
    });
  });
  card.update = update;
  card.update();
  return card;
}

