import mk_quirk from '../functions/mk_quirk';
import name_jumble from '../functions/name_jumble';


export default function(rrpgg,card) {
  if ( card === undefined ) return ['quirk'];

  let quirk_description = mk_quirk(rrpgg);

  card.name = name_jumble(rrpgg,quirk_description);
  card.description_lines.push(quirk_description);

  return card;
}
