export function update(rrpgg, card) {
  card.icon_type = card.icon_type || card.filter;
  let card_to_filter = card.data.card_to_filter;
  if ( card_to_filter ) {
    let filters = card.data.filter.split(' ');
    card_to_filter.children_ids.forEach( id => {
      let child_card = card_to_filter.children[id];
      if ( filters.includes(child_card.type) ){
        card.children[child_card.id] = child_card;
      }
    });
    card_to_filter.connection_ids.forEach( id => {
      let connection_card = card_to_filter.connections[id];
      if ( filters.includes(connection_card.type) ){
        card.connections.push(connection_card);
        card.connections[connection_card.id] = connection_card;
      }
    });
  }
  card.description_lines = [];
  if ( card.children_ids.length ) {
    card.description_lines.push( '- Contains: ' + card.children_ids.map( id => card.children[id].title ).join(', ') );
  }
  if ( card.connection_ids.length ) {
    card.description_lines.push( '- Connected to: ' + card.connection_ids.map( id => card.connections[id].title ).join(', ') );
  }
  card.description = card.description_lines.join('\n');
  return card;
}

export default function(rrpgg, card, config) {
  if ( card === undefined ) return ['filter'];
  card.name = config.name || card.type;
  card.data.filter = config.filter || 'all';
  card.data.card_to_filter = config.card;
  card.icon_type = card.icon_type || config.filter;
  return card;
}
