export default function mk_place(rrpgg, card) {
  if ( card === undefined ) return ['place'];
  let type = card.type;
  let place_type = rrpgg.from_list(type+'_types');
  let pre = rrpgg._chance.bool();
  if ( pre ) {
    let place_special_pre = rrpgg.from_list(type+'_special_pre');
    card.name = place_special_pre + ' ' + place_type;
  } else {
    let place_special_post = rrpgg.from_list(type+'_special_post');
    card.name = place_type + ' ' + place_special_post;
  }
  card.description_lines.push(`- Smells: ${card.data.smells[0]}, ${card.data.smells[1]}, ${card.data.smells[2]}`);

  return card;
}
