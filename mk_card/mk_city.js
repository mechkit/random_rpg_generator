import get_unique_from_list from '../functions/get_unique_from_list';
import get_some_from_list from '../functions/get_some_from_list';
import two_part_name from '../functions/two_part_name';
import to_title from '../functions/to_title';
import lists from '../functions/lists';
// import { building_subtypes } from './mk_building';

const city_subtypes_populations = {
  'hamlet': [0,60],
  'village': [60,200],
  'town': [200,5000],
  'city': [5000,25000],
  'metropolis': [25000,40000],
};

export const city_types = Object.keys( city_subtypes_populations );

export default function mk_city(rrpgg, card, config) {
  if ( card === undefined ) {
    return city_types.map( city_type => 'city.'+city_type );
  }
  card.sub_type = card.sub_type || 'town';
  let city_type = card.sub_type;
  card.name = two_part_name(rrpgg, 'city');
  //console.log('-----------------')
  //console.log(card.name)
  // population
  let [pop_min,pop_max] = city_subtypes_populations[city_type];
  let population = rrpgg._chance.natural({ min: pop_min, max: pop_max });
  card.data.population = population;
  let population_group = rrpgg.card_new('group.character',{name:`Characters of ${card.name}`});
  let num_houses = Math.ceil( population / 4 );
  let num_buildings = Math.ceil( num_houses * 0.1 );
  let num_buildings_key = Math.ceil(num_buildings);
  num_buildings_key = num_buildings_key > 4 ? 4 : num_buildings_key;
  num_buildings_key = num_buildings_key < 1 ? 1 : num_buildings_key;
  // industry
  let industry_name = rrpgg.from_list('city_industry_names');
  let industry = lists.city_industry[industry_name];
  // city districts/groups
  let districts = {};
  // big city?
  // divide city into districts
  let district_num = Math.round(Math.pow(card.data.population,0.42)/10) || 1; // In addition to industrial district
  let districts_to_build = [];
  if ( district_num === 1 ) {
    districts_to_build.push( 'town' );
  } else if ( district_num === 2 ) {
    districts_to_build.push( 'town' );
    districts_to_build.push( 'industry' );
  } else if ( district_num === 3 ) {
    districts_to_build.push( rrpgg.from_list('city_district_downtown') );
    districts_to_build.push( 'industry' );
    districts_to_build.push( 'outskirts' );
  } else if ( district_num === 4 ) {
    districts_to_build.push( rrpgg.from_list('city_district_downtown') );
    districts_to_build.push( 'industry' );
    districts_to_build.push( two_part_name(rrpgg,'city_district') );
    districts_to_build.push( two_part_name(rrpgg,'city_district') );
  } else {
    districts_to_build.push( 'industry' );
    let n = district_num - 1;
    for ( let i=0; i<n; i++ ) {
      districts_to_build.push( two_part_name(rrpgg,'city_district') );
    }
  }
  //console.log(card.data.population)

  districts_to_build.forEach( district_type => {
    let district;
    let district_name;
    if ( districts_to_build.length === 1 ) {
      // no districts, add to "city"
      district = card;
      district_name = card.name;
    } else if ( district_type === 'industry' ) {
      //industry
      let industry_area_pre_name = industry.area_name || industry_name;
      let industry_area_post_name = rrpgg.from_list('city_industry_area');
      district_name = to_title(`${industry_area_pre_name} ${industry_area_post_name}`);
      district = rrpgg.card_new('group.district',{name:district_name});
      card.add_child(district);
    } else {
      // all other districts
      district = rrpgg.card_new('group.district',{name:district_type});
      district_name = district_type;
      card.add_child(district);
    }
    // setup groups for district
    let building_group = rrpgg.card_new('group.building',{name:`Buildings of ${district_name}`});
    district.add_child(building_group);
    let character_group = rrpgg.card_new('group.character',{name:`Characters of ${district_name}`});
    district.add_child(character_group);
    district.data.building_group = building_group;
    district.data.character_group = character_group;
    districts[district_type] = district;
    if ( district_type === 'industry' ) {
      let industry_size = Math.round(Math.pow(population,0.5)/10);
      industry_size = industry_size < 1 ? 1 : industry_size;
      for ( let i = 0; i<industry_size; i++) {
        let industry_building_type = rrpgg.pick_one(industry.buildings);
        let new_building_card = rrpgg.card_new(`building.${industry_building_type}`);
        character_group.add_child(new_building_card.children);
        character_group.add_child(new_building_card.connections);
        population_group.add_child_only(new_building_card.children);
        population_group.add_child_only(new_building_card.connections);
        building_group.add_child(new_building_card);
      }
    }
  });
  let district_names = Object.keys(districts);
  if ( district_names.length === 1 ) {
    let district = districts[district_names[0]];
    district.data.building_group = district;
    district.data.character_group = district;
  }
  let key_buildings = get_unique_from_list(rrpgg, 'city_key_buildings', num_buildings_key);
  key_buildings.forEach( building_name => {
    let new_building_card = rrpgg.card_new('building.'+building_name);
    // add to random district
    let district_name = rrpgg.pick_one(district_names);
    let district = districts[district_name];
    district.data.building_group.add_child(new_building_card);
    district.data.character_group.add_child(new_building_card.children);
    district.data.character_group.add_child(new_building_card.connections);
    population_group.add_child_only(new_building_card.children);
    population_group.add_child_only(new_building_card.connections);
  });
  num_buildings -= num_buildings_key;
  num_buildings = Math.pow(num_buildings,1/2);
  num_buildings = Math.ceil(num_buildings);
  let buildings = get_some_from_list(rrpgg, 'city_common_buildings', num_buildings);
  buildings.forEach( building_name => {
    let new_building_card = rrpgg.card_new('building.'+building_name);
    // add to random district
    let district_name = rrpgg.pick_one(district_names);
    let district = districts[district_name];
    district.data.building_group.add_child(new_building_card);
    district.data.character_group.add_child(new_building_card.children);
    district.data.character_group.add_child(new_building_card.connections);
    population_group.add_child_only(new_building_card.children);
    population_group.add_child_only(new_building_card.connections);
  });

  // population
  let adventurer_num = Math.round(Math.pow(population,0.5)/10);
  for ( let i=0; i < adventurer_num; i++ ) {
    let new_card = rrpgg.card_new('character.adventurer');
    let district_name = rrpgg.pick_one(district_names);
    let district = districts[district_name];
    district.data.character_group.add_child(new_card);
    population_group.add_child_only(new_card);
  }
  let townfolk_num = Math.round(Math.pow(population,0.5)/5);
  for ( let i=0; i < townfolk_num; i++ ) {
    let new_card = rrpgg.card_new('character.townfolk');
    let district_name = rrpgg.pick_one(district_names);
    let district = districts[district_name];
    district.data.character_group.add_child(new_card);
    population_group.add_child_only(new_card);
  }
  // relationships
  //console.log(card.name,'characters:',population_group.children_ids.length);
  population_group.children_ids.forEach( id => {
    let character_card = population_group.children[id];
    let relationship = rrpgg.card_new('relationship');
    let parts_to_play = [...relationship.data.template_variable_names];
    let character_part_name = parts_to_play.splice(0,1);
    relationship.data.template_cards[character_part_name] = character_card;
    relationship.add_connection(character_card);
    //character_card.add_connection_oneway(relationship});
    parts_to_play.forEach( part_name => {
      let relationship_character_id = rrpgg.pick_one(population_group.children_ids);
      let relationship_character = population_group.children[relationship_character_id];
      relationship.data.template_cards[part_name] = relationship_character;
      relationship.add_connection(relationship_character);
      //relationship_character = rrpgg.card(relationship_character,{type:'add_link',card:character_card});
      //relationship_character = rrpgg.card(relationship_character,{type:'add_connection_oneway',card:relationship});
    });
  });


  // cleanup
  district_names.forEach( district_name => {
    delete districts[district_name].data.character_group;
    delete districts[district_name].data.building_group;
  });



  let pre = rrpgg._chance.bool();
  if ( pre ) {
    let city_pre = rrpgg.from_list('city_pre');
    card.data.nickname = city_pre + ' ' + to_title(city_type);
  } else {
    let city_post = rrpgg.from_list('city_post');
    card.data.nickname = to_title(city_type) + ' ' + city_post;
  }
  card.description_lines.push(`- ${card.name}, is known as the ${card.data.nickname}`);
  card.description_lines.push(`- Population: ${population.toLocaleString()}`);
  card.description_lines.push(`- Main industry: ${industry_name}`);
  card.description_lines.push(`- Smells: ${card.data.smells[0]}, ${card.data.smells[1]}, ${card.data.smells[2]}`);
  let card_sounds = get_unique_from_list(rrpgg, 'sounds_city');
  card.description_lines.push(`- Sounds: ${card_sounds[0]}, ${card_sounds[1]}, ${card_sounds[2]}`);

  /*
  card = rrpgg.card(card,{
    type: 'new_card',
    card_type: 'filter',
    name: `People of ${card.name}`,
    filter: 'character',
    card: card
  });
  card = rrpgg.card(card,{
    type: 'new_card',
    card_type: 'filter',
    name: `Buildings of ${card.name}`,
    filter: 'building',
    card: card
  });
  */

  return card;
}
