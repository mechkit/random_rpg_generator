import name_jumble from '../functions/name_jumble';

export const ref_list_names = [
  //'bad_odds',
  'class',
  //'d6',
  'decktet',
  //'decktet_suits',
  //'dust',
  //'enneagram',
  //'even_odds',
  'futhorc',
  //'good_odds',
  //'oblique_strategies',
  //'oddities',
  //'personality_drivers',
  //'playing_card_meaning_rank',
  //'playing_card_meaning_suite',
  'playing_cards',
  //'robots',
  //'rogue_items',
  'story',
  'symbols',
  'tarot',
  'useless',
];

export function update(rrpgg, card) {
  return card;
}

export default function(rrpgg,card,config) {
  if ( card === undefined ) return ['reference'];
  const reference_type = card.sub_type || rrpgg.pick_one(ref_list_names);
  let reference;
  if ( config.name !== undefined ) {
    if ( config.description !== undefined ) {
      reference = [config.name,config.description];
    } else {
      reference = config.name;
    }
  } else if ( config.list_item !== undefined ) {
    reference = config.list_item;
  } else {
    reference = rrpgg.from_list(reference_type);
  }
  if ( reference.constructor === Array ) { // array: name, description
    card.name = reference[0];
    card.description_lines.push( reference[1] );
  } else if ( ['useless'].includes(reference_type) ) { // string: description
    //card.name = 'Useless item';
    card.name = name_jumble(rrpgg,reference);
    card.description_lines.push(reference);
  } else { // string: name
    card.name = reference;
  }
  card.name = card.name || reference_type;
  if ( card.name.split(' ').length > 1 && card.name.split(' ')[0].trim().length === 1 || ['playing_cards','futhorc'].includes(reference_type) ) { // Icon in name
    card.icon = card.name.split(' ')[0].trim();
    card.name = card.name.split(' ')[1].trim();
  }
  return card;
}
