export function update(rrpgg, card) {
  return card;
}

export default function(rrpgg,card,config) {
  if ( card === undefined ) return ['generic'];
  card.name = config.name;
  card.description_lines.push( config.description );
  return card;
}
