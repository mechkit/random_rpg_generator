import lists from '../functions/lists';

export const loot_types = [
  'item',
  'armor',
  'melee',
  'ranged'
];

export default function mk_loot(rrpgg, card, config) {
  if ( card === undefined ) {
    return loot_types.map( loot_type => 'loot.'+loot_type );
  }
  card.sub_type = card.sub_type || 'item';
  let type = card.sub_type;
  let item_type = rrpgg.from_list(`loot_${type}_types`);
  let item_special = rrpgg.from_list(`loot_${type}_specials`);
  if ( lists[`loot_${type}_specials_descriptions`] !== undefined && lists[`loot_${type}_specials_descriptions`][item_special] !== undefined ) {
    let item_special_description = lists[`loot_${type}_specials_descriptions`][item_special];
    card.description_lines.push(`- Special attribute(s): ${item_special_description}`);
  }
  card.name = item_special + ' ' + item_type;

  return card;
}
