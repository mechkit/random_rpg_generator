const dot = require('dot');
import to_title from '../functions/to_title';

export function update() {
  let dot_regex = /\{\{([a-z0-9_]+)\}\}/g;
  let dot_template_input = this.data.template_raw.replace(dot_regex, '{{=it.$1}}');
  let template = dot.template(dot_template_input);
  let template_values = {};
  this.data.template_variable_names.forEach( variable_name => {
    if ( this.data.template_cards[variable_name] !== undefined ) {
      //template_values[variable_name] = this.data.template_cards[variable_name].title;
      template_values[variable_name] = '[[' + this.data.template_cards[variable_name].id + ']]';
    } else {
      template_values[variable_name] = to_title(variable_name);
    }
  });
  this.data.template_values = template_values;
  this.description_lines = [];
  this.description_lines.push( template(template_values) );
  this.description = this.description_lines.join('\n');
}

export default function(rrpgg,card) {
  if ( card === undefined ) return ['relationship'];
  let relationship = rrpgg.from_list('relationships');
  card.name = relationship.title;
  card.data.template_raw = relationship.template;
  card.data.template_variable_names = relationship.parts;
  card.data.template_cards = {};
  card.update = update;
  card.update();
  return card;
}
