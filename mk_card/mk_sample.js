import sort_cards from '../functions/sort_cards';
var Chance = require('chance');

export function update() {
  let neo_chance = new Chance();
  this.connections = {};
  Object.keys(this.data.cards_by_type).forEach( card_type => {
    let sample_card = neo_chance.pickone(this.data.cards_by_type[card_type]);
    this.add_connection_oneway(sample_card);
  });
  this.description_lines = [];
  if ( this.children_ids.length ) {
    this.description_lines.push( '- Contains: ' + this.children_ids.length + ' thiss' );
  }
  this.description = this.description_lines.join('\n');
}

export default function(rrpgg, card, config){
  if ( card === undefined ) return ['sample'];
  let card_to_sample = config.card;
  card.name = 'Sample of ' + card_to_sample.name;
  let cards_by_type = sort_cards(card_to_sample);
  card.data.cards_by_type = cards_by_type;
  //console.log(card_types)
  /*
  const card_types = rrpgg.card_new();
  const number_cards_each_type = 1;
  card_types.forEach( card_type => {
    for ( let i = 0; i < number_cards_each_type; i++ ){
      if ( card_type !== 'sample' ) {
        card = rrpgg.card(card, {type:'new_card', card_type:card_type});
      }
    }
  });
  */
  card.update = update;
  card.update();
  return card;
}

