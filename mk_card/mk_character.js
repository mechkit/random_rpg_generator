import { loot_types } from './mk_loot';

export function update() {
  this.description_lines = [];
  this.description_lines.push( `- Occupation: ${this.data.occupation}` );
  this.description_lines.push( `- Nature: ${this.data.nature}` );
  if( this.data.adjectives !== undefined ) {
    this.description_lines.push(`- Attributes: ${this.data.adjectives[0]}, ${this.data.adjectives[1]}, ${this.data.adjectives[2]}`);
  }
  this.description_lines.push( `- ${this.data.age}, ${this.data.gender}` );
}

export default function(rrpgg, card) {
  if ( card === undefined ) {
    return [
      'character',
      'character.adventurer',
      'character.townfolk'
    ];
  }
  card.name = rrpgg.from_list('names_fantasy');
  card.data.age = rrpgg.age();
  card.data.adventurer = true;
  card.data.gender = rrpgg._chance.gender({extraGenders: ['Non-binary']});

  var class_card = rrpgg.pick_one( rrpgg.library.lists['class'] );
  card.data.class = '[['+ class_card.id +']]';
  card.add_link(class_card);
  var nature_card = rrpgg.pick_one( rrpgg.library.lists['story'] );
  card.data.nature = '[['+ nature_card.id +']]';
  card.add_link(nature_card);
  card.data.occupation = card.data.occupation || card.data.class;
  if ( card.sub_type === 'adventurer' ) {
    loot_types.forEach( loot_type => {
      let new_card = rrpgg.card_new('loot.'+loot_type);
      card.add_child(new_card);
    });
  }
  if ( card.sub_type === 'townfolk' ) {
    let new_card = rrpgg.card_new('tools');
    card.add_child(new_card);
  }
  var quirk = rrpgg.card_new('quirk');
  card.add_child(quirk);
  card.update = update;
  card.update();
  return card;
}
