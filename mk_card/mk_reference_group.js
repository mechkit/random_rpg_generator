function update() {
  return this;
}

export default function(rrpgg,card,config) {
  if ( card === undefined ) return ['reference_group'];
  let list_name = config.list_name;
  card = rrpgg.card_new('group.reference',{name:list_name});
  let list = rrpgg.list(list_name);
  list.forEach( list_item => {
    let ref = rrpgg.card_new('reference.'+list_name, { list_item });
    card.add_child(ref);
  });
  //let reg_group = rrpgg.from_list(list_name);
  card.update = update;
  card.update();
  return card;
}
