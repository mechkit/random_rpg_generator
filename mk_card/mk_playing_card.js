export default function(rrpgg, card){
  if ( card === undefined ) return ['playing_card'];
  //card.name = rrpgg.from_list('playing_cards');
  let rank_array = rrpgg.from_list('playing_card_meaning_rank').split('|').map( str => str.trim() );
  let [rank,rank_meaning] = rank_array;
  card.description_lines.push(`- ${rank}: ${rank_meaning}`);
  let suite_array = rrpgg.from_list('playing_card_meaning_suite').split('|').map( str => str.trim() );
  let [suite,suite_meaning] = suite_array;
  card.description_lines.push(`- ${suite}: ${suite_meaning}`);
  card.name = `${rank} of ${suite}`;
  return card;
}

