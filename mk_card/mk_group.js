export function update() {
  console.log('this',this);
  this.description_lines = [];
  if ( this.children_ids?.length ) {
    this.description_lines.push( '- Contains: ' + this.children_ids.map( id => `[[${id}]]` ).join(', ') );
  }
  if ( this.connection_ids?.length ) {
    this.description_lines.push( '- Connected to: ' + this.connection_ids.map( id => `[[${id}]]` ).join(', ') );
  }
  this.description = this.description_lines.join('\n');
  return this;
}

export default function(rrpgg, card, config) {
  if ( card === undefined ) return ['group'];
  card.name = config.name || card.sub_type || card.type;
  card.data.group_type = config.group_type || card.type;
  card.update = update;
  console.log('xxxx');
  card.update();
  return card;
}
