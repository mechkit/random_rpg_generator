import get_unique_from_list from '../functions/get_unique_from_list';
import to_title from '../functions/to_title';
import lists from '../functions/lists';


const building_specs = {
  'farm': {
    naming_pattern: 'owners',
    occupation: 'farmer'
  },
  'stable': {
    naming_pattern: 'owners',
    occupation: 'groom'
  },
  'wood_yard': {
    naming_pattern: 'owners',
    occupation: 'carpenter'
  },
  'sawmill': {
    naming_pattern: 'owners',
    occupation: 'carpenter'
  },
  'carpenter': {
    naming_pattern: 'the',
    occupation: 'carpenter'
  },
  'town_hall': {
    occupation: 'mayor',
    occupation_worker: 'clerk',
    naming_pattern: 'official'
  },
  'refinery': {
    naming_pattern: 'owners',
    occupation: 'blacksmith'
  },
  'mine': {
    naming_pattern: 'owners',
    occupation: 'miner'
  },
  'shoemaker': {
    naming_pattern: 'the',
    occupation: 'shoemaker'
  },
  'leather_tanner': {
    naming_pattern: 'the',
    occupation: 'tanner'
  },
  'furrier': {
    naming_pattern: 'the',
    occupation: 'furrier'
  },
  'tailor': {
    naming_pattern: 'the',
    occupation: 'tailor'
  },
  'weaver': {
    naming_pattern: 'the',
    occupation: 'weaver'
  },
  'jeweler': {
    naming_pattern: 'the',
    occupation: 'jeweler'
  },
  'chandlers': {
    naming_pattern: 'owners',
    occupation: 'candlemaker'
  },
  'appiary': {
    naming_pattern: 'owners',
    occupation: 'beekeeper'
  },
  'string_maker': {
    naming_pattern: 'the',
    occupation: 'string maker'
  },
  'baker': {
    naming_pattern: 'the',
    occupation: 'naker'
  },
  'vineyard': {
    naming_pattern: 'owners',
    occupation: 'farmer'
  },
  'wine_cellar': {
    naming_pattern: 'owners',
    occupation: 'cellar master'
  },
  'winery': {
    naming_pattern: 'owners',
    occupation: 'farmer'
  },
  'spice_market': {
    naming_pattern: 'owners',
    occupation: 'spicer seller'
  },
  'spice_mixer': {
    naming_pattern: 'owners',
    occupation: 'spice mixer'
  },
  'butcher': {
    naming_pattern: 'the',
    occupation: 'butcher'
  },
  'animal_yard': {
    naming_pattern: 'owners',
    occupation: 'farmer'
  },
  'bookbinder': {
    naming_pattern: 'the',
    occupation: 'bookbinder'
  },
  'paper_maker': {
    naming_pattern: 'owners',
    occupation: 'paper maker'
  },
  'inn': {
    naming_pattern: 'two_part_name',
    occupation: 'innkeeper'
  },
  'blacksmith': {
    naming_pattern: 'two_part_name',
    occupation: 'smith'
  },
  'mill': {
    occupation: 'miller',
  },
  'abandoned': {
    naming_pattern: 'simple',
  },
  'store': {
    naming_pattern: 'store',
    occupation: 'merchant',
  },
  'store-shoe': {
    naming_pattern: 'store',
    occupation: 'merchant/shoemaker'
  },
  'store-weapon': {
    naming_pattern: 'store',
    occupation: 'merchant/weaponsmith',
  },
  'store-book': {
    naming_pattern: 'store',
    occupation: 'book seller',
    sells: 'books',
  },
  'store-armor': {
    naming_pattern: 'store',
    occupation: 'merchant/armorsmith',
  },
  'store-item': {
    naming_pattern: 'store',
    occupation: 'merchant',
  },
  'store-potion': {
    naming_pattern: 'store',
    occupation: 'merchant/brewer',
  },
  'store-general': {
    naming_pattern: 'store',
    occupation: 'merchant',
  },
  'store-farm': {
    naming_pattern: 'store',
    occupation: 'merchant',
  },
  'store-food': {
    naming_pattern: 'store',
    occupation: 'merchant',
  },
};


export var building_types = Object.keys(building_specs);
building_types = building_types.concat( lists['city_key_buildings'] );
building_types = building_types.concat( lists['city_common_buildings'] );
// building_types = Array.from( new Set(building_types) );

export default function mk_building(rrpgg, card, config) {
  if ( card === undefined ) {
    return building_types.map( building_type => 'building.'+building_type );
  }
  // fix missing info
  card.sub_type = card.sub_type || 'abandoned';
  if ( card.sub_type === 'store' ) {
    card.sub_type = 'store-' + rrpgg.pick_one(['weapon','book','armor','item','potion','general','farm','food']);
  }
  // get building specs
  let building_type = card.sub_type;
  let building_spec = building_specs[building_type];
  // owners and workers
  let owner = rrpgg.card_new('character.townfolk');
  card.add_connection(owner);
  let workers = [];
  for ( let i=0; i<1; i++ ) {
    let worker = rrpgg.card_new('character.townfolk');
    card.add_connection(worker);
    workers.push(worker);
    if ( building_spec.occupation_worker ) {
      let occupation = to_title(building_spec.occupation_worker);
      worker.add_data({occupation:occupation,adventurer:false});
    }
  }
  if ( building_spec.occupation ) {
    let occupation = to_title(building_spec.occupation);
    owner.add_data({occupation:occupation,adventurer:false});
  }
  // Start building naming
  // pull building_naming_pattern from object above
  let building_naming_pattern = building_spec.naming_pattern;
  // refine building_naming_pattern
  building_naming_pattern = building_naming_pattern || 'owners';
  if ( building_naming_pattern === 'owners' ) {
    building_naming_pattern = rrpgg.pick_one(['past_owners','owners']);
  }
  if ( building_naming_pattern === 'the' ) {
    building_naming_pattern = rrpgg.pick_one(['the','the','the','past_owners','owners']);
  }
  // critical fail?
  let building_name_roll = rrpgg.dice('d20');
  if ( building_name_roll === 1 ) {
    building_naming_pattern = 'simple';
  }
  // make name
  if ( building_naming_pattern === 'official' ) {
    card.name = to_title(card.sub_type);
  } else if ( building_naming_pattern === 'owners' ) {
    card.name = `${owner.name}'s ${building_type}`;
  } else if( building_naming_pattern === 'the' ) {
    card.name = `${owner.name} the ${building_type}`;
  } else if ( building_naming_pattern === 'past_owners' ) {
    let founder_person_name = rrpgg.from_list('names_fantasy');
    card.name = `${founder_person_name} ${building_type}`;
  } else if ( building_naming_pattern === 'simple' ) {
    card.name = 'the ' + building_type;
  } else if ( building_naming_pattern === 'two_part_name' ) {
    card.name = rrpgg.two_part_name(`building_${building_type}`, ' ');
  } else if ( building_naming_pattern === 'mill' ) {
    var sub_type = card.sub_type || rrpgg.pick_one(['grist', 'corn', 'flour', 'feed', 'cereal', 'grain',]);
    var power_type = card.sub_type || rrpgg.pick_one(['wind','water']);
    card.description_lines.push(`- Powered by: ${power_type}`);
    card.description_lines.push(`- Mills: ${sub_type}`);
    card.description_lines.push('- Produces: flour and middlings');
    card.name = sub_type + 'mill';
  } else if ( building_naming_pattern === 'store' ) {
    var store_type = card.sub_type.split('-')[1];
    // card.icon_type = 'store-' + store_type;
    let sells = building_spec.sells || store_type;
    card.description_lines.push(`- Sells: ${sells}`);
    let store_naming_pattern = rrpgg.pick_one(['person_store', 'person_type','of','simple']);
    if ( store_naming_pattern === 'simple' ) {
      card.name = store_type + ' store';
    } else if ( store_naming_pattern === 'of' ) {
      let of_prefix = rrpgg.pick_one(['world', 'lots', 'store']);
      card.name = of_prefix + ' of ' + store_type + 's';
    } else if ( store_naming_pattern === 'person_store' ) {
      card.name = `${owner.name}'s store`;
    } else if ( store_naming_pattern === 'person_type' ) {
      card.name = `${owner.name}'s ${store_type}`;
    }
  } else {
    card.name = building_type;
  }

  card.title = to_title(card.name);

  card.description_lines.push(`- Smells: ${card.data.smells[0]}, ${card.data.smells[1]}, ${card.data.smells[2]}`);
  let card_sounds = get_unique_from_list(rrpgg, 'sounds_city');
  card.description_lines.push(`- Sounds: ${card_sounds[0]}, ${card_sounds[1]}, ${card_sounds[2]}`);
  //card.description_lines.push(`- Owned by ${owner.title}`);
  card.description_lines.push(`- Owned by [[${owner.id}]]`);
  ///owner = rrpgg.card(owner, {type:'append_description',line:`- Owns ${card.title}`});
  owner.append_description(`- Owns [[${card.id}]]`);
  let work_id = [];
  workers.forEach( worker => {
    worker.append_description(`- Works at [[${card.id}]]`);
    work_id.push( '[[' +  worker.id + ']]' );
  });
  card.description_lines.push(`- Workers ${work_id.join(', ')}`);


  return card;
}
