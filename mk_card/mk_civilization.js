import { city_types } from './mk_city.js';

export default function(rrpgg, card){
  if ( card === undefined ) return ['civilization'];
  card.name = rrpgg.two_part_name('city');
  var adjective = rrpgg.from_list('adjectives_city');
  card.description_lines.push(`The civilization of ${card.name} is a ${adjective} place.`);
  city_types.forEach( city_type => {
    let city = rrpgg.card_new('city.'+city_type);
    card.add_child(city);
  });
  return card;
}

