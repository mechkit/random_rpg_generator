import lists from '../functions/lists';


const tool_kit_types = [
  'carpenter',
  'cartographer',
  'cobbler',
  'glassblower',
  'jeweler',
  'leatherworker',
  'mason',
  'potter',
  'smith',
  'tinker',
  'weaver',
  'woodcarver',
  'navigator',
  'thieves',
];

const supply_types = [
  'alchemist',
  'brewer',
  'calligrapher',
  'painter',
  'cook',
];

let enetertainment = [
  'Dice set',
  'Playing card set',
];

let instruments = [
  'bagpipes',
  'drum',
  'dulcimer',
  'flute',
  'lute',
  'lyre',
  'horn',
  'pan flute',
  'shawm',
  'viol',
];

export default function mk_loot(rrpgg, card, config) {
  if ( card === undefined ) {
    return tool_kit_types.map( loot_type => 'tools.'+loot_type );
  }
  card.sub_type = card.sub_type || 'tools';
  let tool_type = rrpgg.pick_one(tool_kit_types);
  if ( tool_type[tool_type.length-1] === 's' ) {
    tool_type += '\'';
  } else {
    tool_type += '\'s';
  }
  tool_type += ' tools';
  let item_quality = rrpgg.from_list('loot_quality');
  card.name = item_quality + ' ' + tool_type;
  // card.sub_type = tool_type;
  if( card.data.adjectives !== undefined ) {
    card.description_lines.push(`- Attributes: ${card.data.adjectives[0]}, ${card.data.adjectives[1]}, ${card.data.adjectives[2]}`);
  }
  return card;
}
