import lists from './functions/lists';
import get_unique_from_list from './functions/get_unique_from_list';
import card_proto from './functions/card_proto';

import mk_building from './mk_card/mk_building';
import mk_character from './mk_card/mk_character';
import mk_city from './mk_card/mk_city';
import mk_civilization from './mk_card/mk_civilization';
import mk_filter from './mk_card/mk_filter';
import mk_group from './mk_card/mk_group';
import mk_loot from './mk_card/mk_loot';
import mk_place from './mk_card/mk_place';
import mk_playing_card from './mk_card/mk_playing_card';
import mk_relationship from './mk_card/mk_relationship';
import mk_sample from './mk_card/mk_sample';
import mk_task from './mk_card/mk_task';
import mk_reference_group from './mk_card/mk_reference_group';
import mk_reference from './mk_card/mk_reference';
import mk_generic from './mk_card/mk_generic';
import mk_quirk from './mk_card/mk_quirk';
import mk_category from './mk_card/mk_category';
import mk_tools from './mk_card/mk_tools';

// import { building_types } from './mk_card/mk_building';
// import { city_types } from './mk_card/mk_city';
// import { loot_types } from './mk_card/mk_loot';

let card_functions = {
  building: mk_building,
  character: mk_character,
  city: mk_city,
  civilization: mk_civilization,
  filter: mk_filter,
  group: mk_group,
  loot: mk_loot,
  place: mk_place,
  playing_card: mk_playing_card,
  relationship: mk_relationship,
  sample: mk_sample,
  task: mk_task,
  reference_group: mk_reference_group,
  reference: mk_reference,
  generic: mk_generic,
  quirk: mk_quirk,
  category: mk_category,
  tools: mk_tools,
};
card_functions['custom'] = (rrpgg, card, config) => {
  if ( card === undefined ) return ['custom'];
  card.name = config.name;
  config.description = config.description || 'custom card';
  if ( config.description.constructor === Array ) {
    card.description_lines = config.description;
  } else {
    card.description_lines.push( config.description );
  }
  delete config.name;
  delete config.description;
  // TODO: find good merge function
  config.data = config.data || {};
  card.data = card.data || {};
  card.data = Object.assign(card.data,config.data);
  card = Object.assign(card,config);
  return card;
};
card_functions['void'] = (rrpgg, card) => {
  if ( card === undefined ) return ['void'];
  card.description_lines.push('...nothing     here...');
  return card;
};
card_functions['world'] = (rrpgg, card) => {
  if ( card === undefined ) return ['world'];
  card.description_lines.push('Welcome to The World');
  return card;
};
let card_functions_names = Object.keys(card_functions);

/*
let card_types = [...card_functions_names]
  .concat(loot_types.map( loot_type => `loot.${loot_type}`))
  .concat(city_types.map( city_type => `city.${city_type}`))
  .concat(building_types.map( building_type => `building.${building_type}`));
*/
let card_types = Object.keys(card_functions).map(id=>card_functions[id]).reduce( (card_types,card_function) => {
  return card_types.concat( card_function() );
}, [] );

export default function(rrpgg, raw_type, config){
  if ( raw_type === undefined ) {
    return card_types;
  }
  let type;
  let sub_type;
  if ( raw_type && raw_type.split('.').length > 1 ) {
    let type_parts = raw_type.split('.');
    type = type_parts[0];
    sub_type = type_parts[1];
  } else {
    type = raw_type;
  }
  let card = Object.assign( Object.create(card_proto), { // so much clearer than "new Card({"
    id: config.id,
    type,
    sub_type,
    description_lines: [],
    data: {},
    icon_type: undefined,
    parents: {},
    children: {},
    connections: {},
    links: {},
    backlinks: {},
    update_function: undefined,  // function in data??
    rrpgg, // better place to store??
  });
  card.update();

  card.data.smells = get_unique_from_list(rrpgg, 'smells');
  if ( card_functions_names.includes(type) ) {
    card = card_functions[type](rrpgg, card, config);
  } else {
    return card_types;
  }
  let adjective_word_list_name = `adjectives_${type}`;
  if ( lists[adjective_word_list_name] !== undefined ) {
    card.data.adjectives = get_unique_from_list(rrpgg, adjective_word_list_name);
    card.description_lines.push(`- Attributes: ${card.data.adjectives[0]}, ${card.data.adjectives[1]}, ${card.data.adjectives[2]}`);
  }
  card.name = card.name || config.card_name || card.type;
  card.title = card.title || card.name;
  // card.icon_type = config.icon_type || card.icon_type || card.sub_type || card.type;
  card.icon_type = config.icon_type || card.icon_type || raw_type;
  card.icon = config.icon || card.icon || undefined;
  card.description = card.description_lines.join('\n');
  //card = rrpgg.card(card);
  card.update();
  return card;
}

