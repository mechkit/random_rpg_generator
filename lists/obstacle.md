task:
- deliver
- clear
- escort
- find


obstacle:
- late
-

The 36 Plots
by Loren J. Miller
This article is descended from an article I wrote several years ago about plotted games, based on my interpretation of a book by Georges Polti called [The Thirty-Six Dramatic Situations](http://www.amazon.com/exec/obidos/ASIN/0871161095/rpglibrary). There are several other improvisations on my article floating around the internet, and at least one independently written article similar to it.

Assuming a RPG is like a story, what are the different structural parts of the story and how do they work in the RPG?

Those who argue against storytelling within rpgs seem to say the GM's only jobs are setting and character (and maybe dialogue) and plot isn't important, much like slice of life writers argue with more traditional writers over the proper structure of short stories. With this I hope to make the job of writting a plotted game that allows for free actions by player characters not only possible, but simple.

Each short plot description starts with the title of the plot pattern. After a hyphen the main characters to be found in the plot are given, separated by commas.

RPG scenarios IMHO too often tend to be Daring Enterprises: The PCs bravely go on a quest to bring back some priceless relic and enough gold to give a hundred horses hernias. I like to use other plots though, they make me think up better, more original, gaming scenarios.

To use these things, sometime in your planning, pick one of the 36 plots to use for your adventure. Then choose the main characters who are necessary for that plot. Say you choose Ambition as the basic plot (how Shakespearean :-) and decide that the players are going to be the ambitious person(s) (not a far stretch for most PCs). The Adversary is to be the main opposition to the PCs, so make it a wealthy, traveled businessman with his own bodyguards and quite a bit of fighting skill himself, so the PCs won't kill him out of hand to solve their problem.

Also give him good features, something that will make the players sympathize with him. Maybe he's the father of a boy or girl who is in love with one of the PCs. The coveted thing is pretty easy to figure, find something that one of the PCs wants enough to start laying plans to gain it. Also give the ambitious PC a friend who supports her ambition and keeps tempting her further into the messy situation.

_Example: Most campaigns have a player who loves to play politics, involve her in this. Assume for the sake of argument that the goal is the office of district attorney. Enigma has ambitions to be the DA, the chief force for justice in Gotham. He is opposed by Buck Stevens, son of the founder of Stevens Brick Co., which is the second largest employer in Gotham. Darla Stevens is in love with the Enigma's alter ego, Bing Strawberry, and keeps telling him he ought to get in politics and make sure her slimy brother doesn't achieve political office ... etc etc etc you get the idea._

That's a skeletal plot, right there, but it's enough to guide the rest of the adventure. The acts in the plot almost write themselves:

Complications can be created by the GM from various PCs' friends and enemies, and several very interesting ethical dilemmas can be emphasized in the game, making this type of scenario wonderful ground for role-playing, and a natural for fisticuffs and other conflict (though it would cause big problems with the press and voters if the candidate hurt or killed someone, or were even suspected of it!).

The table of plots below has 36 rows, one for each of Polti's canonical plots, and 4 columns. The first column is the plot number. The second column is the plot name. The third column lists the important actors and/or elements in the plot. And the fourth column includes one or more brief plot summaries, of no more than a single paragraph.

No. Plot Name Actors and Elements Brief Plot Summary 1 Supplication

1.  Persecutor
2.  Supplicant
3.  Power in Authority

Under persecution, a village sends out emissaries to the local lord, who is not known for his kindness, to beg for relief from bandits. They are harassed by the bandits along the way.

2 Deliverance

1.  Unfortunates
2.  Threatener
3.  Rescuer

Bandits have been preying on a village and threaten to burn its crops. Come save the village and destroy the bandits.

3 Revenge

1.  Righteous Avenger
2.  Criminal

A PC's sibling has been killed accidentally in a raid. His wife refuses to accept the rule of law, which states that she should accept a weregeld for him. She involves several PCs in her plotting to totally annihilate the family of the killer, including parents, siblings, children, and siblings' children.

4 Vengeance by Family upon Family

1.  Avenging Kinsman
2.  Guilty Kinsman
3.  Relative

One of a PC's kin killed another in confused circumstances. Whether it was accidental or not is unclear. The head of the family makes the killer responsible for supporting the bereaved spouse and children. But the widowed spouse is unwilling to accept aid from a fratricide, and involves PCs in plots to destroy the new guardian.

5 Pursuit

1.  Fugitive
2.  Pursuer

A madman wanders into town, raving about the monsters that pursue him and his poor, lost love. He is obviously high born, and needs to be taken in. A few days later a squad of pursuers arrives, to find and arrest him for crimes against humanity and his own family, led by his advisor whose advice drove him quite mad, to this awful juncture. Imagine this as a sequel to Othello, with a jealous Iago pursuing tragic Othello to bring him back to justice.

6 Victim of Cruelty or Misfortune

1.  Unfortunates
2.  Indifferent or Cruel Master

A cruel but effective noble is disgraced after an intrigue and loses his titles, possessions and lands to an equally cruel rival. He has been banished and blinded, and is wandering, alone, without followers, pitiful, when he encounters the PCs.

7 Disaster

1.  Vanquished Power
2.  Victorious Power
3.  Messenger

The country has been invaded and the flower of this country's knighthood has been slain. The army of the enemy is advancing, burning fields and robbing and murdering the peasants. Run for your lives!

8 Revolt

1.  Tyrant
2.  One or more Conspirators

A well-respected noble enlists the PCs to assist in a plot to overthrow his own lord and usurp his position. His intentions seem respectable at first, but grow murkier with time.

9 Daring Enterprise

1.  Goal
2.  Bold Leader
3.  Adversary

Raikiela is an artist, and has promised her masterpiece to the prince. Unfortunately, her friend showed the masterpiece to a foreign noble, who desired it and took it without permission. Without starting a war, the PCs must organize an expedition to get the masterpiece back from the foreigner and to the prince.

10 Abduction

1.  Abductor
2.  Abducted
3.  Guardian

Pitolso is in love with Kareta, and Kareta is in love with Pitolso. Pitolso enlists the PCs to help steal Kareta away from those who would keep Kareta for themselves. They must abduct Kareta without killing anybody and thus rousing the implacable forces of justice. Kidnapping? No, rather liberating the love from those who have imprisoned him/her.

11 Mystery or Enigma

1.  Interrogator
2.  Seeker
3.  Problem

Fandolio has fallen in love with Ilsentosa, the princess and heir to the throne, and she has announced that if he wants to win her love then he must promise to solve her riddle, and if he cannot solve her riddle then he must die. He has accepted the deal, and enlists the PCs to help him find the solution. The riddle is...

12 Obtaining

1.  Goal
2.  Several Opposed Groups
3.  Judge or Referee

It's time for the Spring Pageant! The maidens engage in contests to see who is the most beautiful, the most religious, the most knowledgeable about lineage, and the most charming speaker. The contest is judged by former Year Queens. The winner is crowned the Year Queen and gets to marry the Great Hunter!

It's time for the Great Hunt! Unmarried men are able to enter the contest. Each contestant must go out into the wild with only a knife, a spear, and a bow and arrow, no armor. Come back with the best catch, alive if possible. The contest will be judged by former winners of the Great Hunt. The winner is crowned the Great Hunter and gets to marry the Year Queen.

13 Familial Hatred

1.  Two or more Family members who hate each other

A PC's brother hates his sister's husband, whom he sees as a whimpering syncophant. The brother-in-law in turn sees the PC's brother as a cruel bully. The PC's brother has told the PC in private, after swearing him to secrecy, that he has taken a vow to kill his brother-in-law.

14 Familial Rivalry

1.  Preferred Kinsman
2.  Rejected Kinsman
3.  Object

One of the player characters and his step-brother Joli, a friendly sort of guy, have both been taken as lovers by the same merchant's wife, who hides the affairs from her husband, encourages both lovers, and makes them intrigue against each other and her husband, at the same time planting seeds of suspicion in her husband, who is very jealous.

15 Murderous Adultery

1.  Adulterer
2.  Cuckold
3.  Adulterer's Lover

A married friend begins to have an adulterous affair with a mysterious woman. This gives him new zest for life. Then his wife disappears, and the rumors start. A week later, the PCs are accosted by her ghost, demanding proper burial for her missing body (she doesn't know where it is) and vengeance on the adulterers. Try not to get framed for her murder along the way.

16 Madness

1.  Madman
2.  Victim

The local hospital begins to be the site of strange, horrible, unexplained deaths. Then a PC's relative dies from a minor ailment. Octara, one of the healers at the hospital has gone mad, and sometimes, for some insane reason, slays her patients instead of healing them. Usually Octara appears to be perfectly normal and sane.

The Bloody Bull, the PCs' favorite tavern, and Azracon their favorite tavern-keeper, have been there for years, serving them good food and drink. One day Azracon changes the sign to a Happy Horse and brings two horses in and seats them at tables. Within a week, all the rooms are full of horses that he has purchased. Within another week, you cannot get a place at his tables because the entire tavern is full of horses. Then Azracon announces to everyone on the street that he is going to marry Pythalda, the most beautiful maid in the world, and is looking for a seamstress to sew a horse-sized and horse-shaped wedding dress.

or...

The Bachelor Prince Prophoc has always been a keen horseman. Then one day he decides to seat his favorite horse at the dinner table, displacing a foreign dignitary. This is a terrible scandal. A week or so later, Prophoc holds a dinner at which he serves all sorts of roasted delicacies to over a score of horses, and in a rage he kills a horse trainer who complains that the horses won't and can't eat the food. Soon the prince's mansion and grounds are filled with horses, and taxes are assessed to pay for the drain on the treasury. The people of the city are about to rise and overthrow him, when he declares that he has fallen in love, and that he will marry. There is much rejoicing, and it seems that things have gotten better, since the number of horses in the prince's mansion falls to normal levels. Then, on the wedding day, he forces the city's high priest, with death the penalty for refusal, to marry him to his favorite white mare.

17 Fatal Imprudence

1.  Imprudent Person
2.  Lost Object
3.  Victim

A character known to the player characters, and which we recommend be one of the PCs, has a famous item, which may or may not be magical. A curious family member was playing with it, took it out of the house, and carried it around for a while, gradually growing bored of it, and after being attracted to some unusual noise and then frightened by seeing something s/he shouldn't have seen, drops the item and runs home, where s/he is found later, shivering in fear. The guard finds the famous item at the scene of a horrifying murder, and as a result the PC becomes the main suspect.

18 Involuntary Crimes of Love

1.  Lover
2.  Beloved
3.  Revealer

Bonchanto, a friendly but mischievous acquaintance of one of the PCs, introduces him to Demice, a beautiful young woman of respectable family, who has much in common with the PC, but takes some pains to make sure that the PC doesn't meet Demice's parents. At the same time he offers to act as a go-between for the lovers. He arranges for them to meet in secret several times. Soon thereafter he leaves town. Then a letter from Bonchanto arrives at Demice's home, and all hell breaks loose. Her parents come down on the PC like a ton of bricks. Here's why... Nearly 20 years ago, Demice's supposed parents couldn't bear children of their own, and so they asked the PC's parents to adopt one of their children. This wish was granted. The PC has taken his own sister as his lover!

19 Kinsman Kills Unrecognised Kinsman

1.  Killer
2.  Unrecognized Victim
3.  Revealer

Politics are getting nasty again, and the loyal opposition have a hooded leader who has been rabble rousing against the main PC's family interests with some effect. One of the PCs is dispatched to a meeting with the secretive leader to try to negotiate or force a solution. Negotiations are not going well, when the city forces decide on that time to raid the meeting and frame or kill the secret leader. They give the secret signal to the PC, the signal to withdraw, and when this happens the secret leader suddenly starts fighting to reach the PC, shouting loudly with his sword flashing wildly. No matter what the PC does, whether the secret leader is rescued or not, whether he is killed or arrested (if the PC does nothing the masked one will be killed in the fracas), when he is unmasked he will be found to be the PC's adoring younger brother.

20 Self Sacrifice for an Ideal

1.  Ideal
2.  Hero
3.  Person or Thing Sacrificed

After declaring that only through sacrifice will our land embody IDEAL, a very well respected and loved elderly couple throw themselves onto a bonfire (with spectacular but unclear magical consequences). This would work best if the elders were either priests at a temple the PCs attend, or grandparents or parents of one or more of the PCs. The authorities, ignorant of the planned sacrifice, will have already dispatched troops to break up the gathering and arrest the leaders.

21 Self Sacrifice for Kindred

1.  Hero
2.  Kinsman
3.  Person or Thing Sacrificed

Mideratho has always been a short-breathed, wimpy, genius. He can understand the workings of mechanisms like nobody's business, and has even invented his own minor sorcerous cantrip to better play with mechanical creations. To his misfortune his parents wish to have a war hero as a son. In order to make his parents happy, Mideratho joins the most hazardous duty military unit he can, and must now learn to fight. Does he lose his sight or a hand in combat? Does he get killed? Can the PCs train him well enough so he can survive? Will he ever get to work on mechanisms again?

22 All Sacrificed for Passion

1.  Hero
2.  Object of Passion
3.  Person or Thing Sacrificed

The Bachelor Prince Santus, the greatest general the land has had in a generation, has conquered two neighboring states by the age of 18, but has been otherwise sheltered from life. He falls in love with Oclea, a prostitute hired by the terrible King Rometradi to seduce him, and completely loses interest in war and his principality, except as a tool which he will spend freely to capture Oclea (who may like him or may despise him, signals are mixed) from Rometradi's land. And worse, he declares he will marry her and make her the queen, and if he can't then he will abdicate his throne to Aelus, his idiot brother.

23 Sacrifice of Loved One

1.  Hero
2.  Beloved Victim
3.  Cause for Sacrifice

Politics are getting nasty again, and the loyal opposition have a hooded leader who has been rabble rousing against the main PC's family interests with some effect. The PC discovers, perhaps he is told by his parent, that the hooded leader is his own, adoring, younger brother Antigoly. The PC is dispatched to a meeting with Antigoly to try to negotiate or force a solution. After the initial meeting fails, the PC is told to do whatever it takes to stop the enemy movement. At the same time, city officials inquire about the location of the enemy leader, and place a bounty on his head. Antigoly refuses to meet with or speak to his family and their agents, including the PCs.

24 Rivalry Between Superior and Inferior

1.  Superior
2.  Inferior
3.  Object

Suddenly Stranoc the Guiding Hand, the most learned magical adept of the land, is at magical war with his oldest friend and former student Vulfus the Black Spider over possession of ARTIFACT. The PCs, who have dealt with both, have no choice. They are involved without their own consent, and must deal with all manner of horrible monstrosities as they attempt to survive, save their friends and families, and get out of town.

**Rivalry of Two Peoples:** Referring back to plot 10, the land was recently conquered by a foreign people, and the High Lord sees the opportunity to marry Kareta to Frusta, a noble of his own people, as a way to legitimize the conquest. He will not give his approval to Kareta and Pitolso, who has kidnapped Kareta heedless of the consequences. Frusta sees her abduction as an opportunity to rescue her from bandits and thus prove his worth in her eyes.

25 Adultery

1.  Deceived Spouse
2.  Adulterer
3.  Adulterer's Lover

Thossuleo sends his wife, Fonya, to visit her family, sells most of his stuff, and goes wild buying baubles for a beautiful young girl, Crotia, who may or may not care for him. She is happy to accept his gifts, however.

Thossuleo loves his wife, Fonya, very much. Then the persecution from Crotiadek, a noble and judge, begins. Thossuleo loses money, status, and finally his freedom. After he is jailed his wife divorces him and moves into Crotiadek's home.

26 Crimes of Love

1.  Lover
2.  Beloved
3.  theme of Dissolution

Procsoutia falls in love with her twin sons Abon and Aromed, and causes them to lay with her. The results of these matings are monstrous and terrible.

27 Discovery of Dishonor of a Loved One

1.  Guilty Beloved
2.  Discoverer

Lyclus and Lia are very proud of their successful daughter Linbaria. Then evidence is found that she has been an assassin working for an evil noble.

Linbaria is very proud of her parents Lyclus and Lia. Then she discovers evidence that Lia had once been a prostitute. Even worse, the evidence indicates that Lia has returned to her former profession in secret.

28 Obstacles to Love

1.  Lover 1
2.  Obstacles
3.  Lover 2

Nymus and Teropha are terribly in love with each other. However, because of their incompatible temperaments every time they make plans to marry they get in a huge fight and end up calling off the engagement. Every time they do this they involve all their friends and end up getting mad at all of them.

Nymus and Teropha are terribly in love with each other. However, their parents do not approve, and have both placed very difficult requirements (quests) upon their children's beloved. In order to marry, Nymus must give Teropha's parents a sieve full of Styx water, and Teropha must produce a wedding dress made of purest starlight.

29 An Enemy Loved

1.  Beloved Enemy
2.  Lover
3.  Hateful Kinsman

Montirkul and Pytha have fallen in love despite their families being at war. The greatest warriors in each of the families are Katal and Shokalmor, whose bitter rivalry will destroy the lovers and tear their families apart.

30 Ambition

1.  Ambitious Person
2.  Coveted Object
3.  Adversary

Fleon is an ambitious general who desires to rule the city and establish a royal line. His wife, Paria, acts as his conscience and to stay his hand as he spins plots and dispatches troops to bring the mercantile and noble interests under his thumb. Finally, Fleon becomes the city chief consul, and has made enough powerful friends to change the charter of the city from a republic to a dictatorship, and Paria must act now if she would stem his ambition.

Prince Mepilus wishes to become king, but his father, King Promanast, shows no sign of weakening, and even worse his older brother, Crown Prince Aphorphon, has been declared the heir apparent. Mepilus hatches a plan to have Aphorphon slay Promanast unknowingly, upon which terrible occasion Mepilus will declare martial law, avenge his father upon his brother, and take the crown of the city for his own.

31 Conflict with a God

1.  Mortal
2.  Immortal

General Panmodon has been arrested, and when he is banished for his crimes he swears bloody vengeance on his home city and on the city's patron god, Lys. He goes away and begins to gather horrible artifacts with the goal of using them to bring plague, war, and famine to the city and thus weaken Lys, so that he can finally destroy Lys's temple and slay the god itself.

The Noble Sturix begins a persecution against the pacifist followers of a new god named Aes. He believes that their religion will lead to disobedience of the civic cult, rebellion against the civil authorities, famine, war, plague, and pestilence, and so he persecutes them vigorously, putting as many of them up on crosses as he can find.

32 Mistaken Jealousy

1.  Jealous One
2.  Object of Jealousy
3.  Supposed Accomplice
4.  Author of Mistake

Medarion is baselessly jealous of his wife Clexia, and continues to grow more and more jealous of her. He persuades Hametrea to infiltrate her group of friends and search out her "many" infidelities. Clexia has two doubles in the populace, one is a poor woman, and the other is a visiting youth who has disguised himself as a woman to hide from pursuers. Medarion himself has also has doubles, a sorceror with the visiting circus and his drooling idiot twin brother.

Medarion discovers his wife Clexia is having an affair with his best friend, Promurtin. Medarion feigns ignorance, but encourages Promurtin to be jealous of Clexia by talking about how much Clexia speaks of her old friend Nantius, and how little she speaks of Promurtin. At the same time Medarion tells Clexia stories about how Promurtin has been seen about town with Nantius and a prostitute named Hametrea.

33 Faulty Judgement

1.  Mistaken One
2.  Victim of Mistake
3.  Author of Mistake
4.  Guilty Person

Oulitarth has been consorting with prostitutes and in order to protect him from being disinherited by his very conservative family, his friend Taverion takes the blame. Taverion's wife, Marnasia, hears of this and leaves him.

34 Remorse

1.  Culprit
2.  Victim
3.  Interrogator

Samarand has led a bad life, and now he is haunted by the ghosts of his misdeeds, which in reminding him of his crimes cause him incredible pain and terror. He finally has remorse, and while still haunted, begins to make some amends to his victims. Please help him with amends, and with the ghosts which terrorize him.

35 Recovery of a Loved One

1.  Seeker
2.  One Found

Mantraeas learns from an oracle that his father, once thought dead, is still alive. Now he will seek his father out.

Marnady learns from an oracle that she has a twin sister, and that this twin sister is still alive. She must seek her sister out and bring her home.

36 Loss of a Loved One

1.  Kinsman Slain
2.  Kinsman Witness
3.  Executioner

Nyoquilat has taken a vow of chastity, and one day he comes home to the horrible sight of his family being slaughtered like sheep by horned raiders. He runs in to try and save them, and is beaten and left for dead, yet he lives. Now he asks for aid from any who would give it.

