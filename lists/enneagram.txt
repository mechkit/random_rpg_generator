- Reformer, Perfectionist
- Helper, Giver
- Achiever, Performer
- Individualist, Romantic
- Investigator, Observer
- Loyalist, Loyal Skeptic
- Enthusiast, Epicure
- Challenger, Protector
- Peacemaker, Mediator
