# Story prompts

| Sym. | Name          | Event                                                                                             | Character                       |
| ---  | ---           | ---                                                                                               | ---                             |
| B    | family        | love, friendship, connection, home, safety, rest                                                  | friendly                        |
| Y    | enemy         | opposition, opponent, obstacle, danger, risk                                                      | hostile                         |
| C    | moon          | night, hidden, stealth, hunt, night                                                               |                                 |
| R    | sun           | day, energy, warmth, illumination, hope, optimism,                                                |                                 |
| E    | labor         | work together, helping others, community, good                                                    |                                 |
| V    | evil          | addiction, bad habits, greed, hatred                                                              |                                 |
| S    | river         | flow, motion, unstoppable, travel, voyage, passageway                                             |                                 |
| D    | dam           | on hold, nothing happening, stuck, letting go, blocked                                            |                                 |
| U    | shield        | protection, defense                                                                               |                                 |
| F    | tool          | advantage, equipment, persuasion                                                                  |                                 |
| P    | forest        | lost                                                                                              |                                 |
| Q    | objective     | in the right place, accomplishment, completion, here                                              |                                 |
| G    | maze          | quest, direction, drive                                                                           |                                 |
| Z    | nomad         | no objective, wanderer, unknown location                                                          |                                 |
| H    | rebirth       | death, spring, forgiveness, endings and new beginnings, transformation, transition, nature        |                                 |
| T    | balance       | measurement, justice, cause and effect                                                            |                                 |
| I    | wizard        | learning path, wisdom, magic, spirituality, mentor, guide                                         |                                 |
| N    | apprentice    | learning, inexperienced                                                                           |                                 |
| K    | fool          | comedy, risk, good luck, destiny, risk, getting outside your comfort zone                         |                                 |
| O    | pond          | stillness, lack of action, serenity, knowing oneself, solitude                                    |                                 |
| M    | monster       | confidence, drive, determination, strength, recklessness                                          |                                 |
| X    | crossroads    | change, anxiety, insecurity, anxiety, opportunity                                                 | anxious, insecure               |
| J    | trap          | surprise, bad luck, unexpected failure, poison, disease, tragedy, destruction, violence, argument | hateful, untrustworthy, violent |
| L    | harvest       | return on investment, yield from past work, treasure                                              | generous, giving, helpful       |
| A    | order/compass | precision, map, order, power, structure, organization, rules                                      | organized, precise              |
| W    | chaos         | destruction, upheaval, unexpected and possibly unwanted change.                                   | chaotic                         |

