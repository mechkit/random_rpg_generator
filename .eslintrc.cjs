module.exports = {
  'env': {
    'browser': true,
    'node': true,
    'es6': true
  },
  'extends': [
    'eslint:recommended',
  ],
  'parserOptions': {
    'ecmaVersion': 'latest',
    'sourceType': 'module'
  },
  'plugins': [
  ],
  'rules': {
    'semi': ['error', 'always'],
    'indent': ['error', 2],
    'quotes': ['warn', 'single'],
    'no-unused-vars': ['warn'],
    'prefer-const': ['off'],
  },
  'overrides': [
  ]
};
